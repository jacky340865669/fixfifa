// Here You can type your custom JavaScript...
$(document).ready(function(){
  console.log( '{maskedDefId: ' + $('#page-info').attr('data-baseid') + ' , maxb: ????} // ' + $('.player_header .header_name').first().text() );
});


let rows = Array.from(document.querySelectorAll('#repTb > tbody > tr'))
let prices = {}

for(let row of rows){
  try{
    let imgSrc = row.querySelector('td.table-row-text.row > div:nth-child(1) > img').getAttribute('data-original')

    let playerId = /.*\/players\/([0-9]+)\.png.*/.exec(imgSrc)[1];

    let title = row.querySelector('td.table-row-text.row > div.d-inline.pt-2.pl-3 > div:nth-child(1) > a');

    title.innerText += ` (${playerId})`;

    let price = row.querySelector('td:nth-child(5) > span').innerText

    let [_, count, unit] = /([0-9]+\.*[0-9]*)(K?)/.exec(price)

    let priceDollars = (unit == 'K') ? (parseFloat(count) * 1000) : parseFloat(count)

    if( priceDollars && priceDollars > 0){
      prices[playerId] = priceDollars;
    }

  }catch(e){
      console.log(e);
  }
}

console.log(prices);