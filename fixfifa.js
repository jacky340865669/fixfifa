require('dotenv').config();
const puppeteer = require('puppeteer');
const readline = require('readline');

(async () => {
  async function handleGenericError(error){
    console.error(error);
    await page.screenshot({path: 'error.png'});
    console.log('error screenshot taken, closing broser....');
  };

  async function askCode(){
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    return new Promise((resolve) => {
      rl.question('GIVE ME THE CODE!!!!!!!!!!! ', (code) => { resolve(code) })
    })
  };

  async function fillSecurityCode(){
    // Security Code
    await Promise.all([
      page.waitForNavigation(),
      page.click('a#btnSendCode')
    ]);

    const code = await askCode();

    await page.type('input#oneTimeCode', code, {delay: 100});

    await Promise.all([
      page.waitForNavigation(),
      page.click('a#btnSubmit')
    ]);

    console.log('Security code redirection done');
  }

  async function answerSecurityQuestion(){
    console.log('Arrived Security Question Section');

    await page.type('input.textInput', process.env.SECURITY_ANSWER, {delay: 100});

    console.log('Finished Security Question Redirection');

    await page.screenshot({path: 'screenshot.png'});

    await page.click('.ut-login-content button.call-to-action');

    await page.waitForFunction(function(){
      return $('.ut-login-content > h2').text().trim() == 'User Agreement';
    });

    console.log('Arrived User Agreement Section');

    await page.mouse.move(100, 100, {steps: 100});

    // wait for login button to enable
    await page.waitForFunction(function(){
      return $('.ut-login-content > button.call-to-action').length && !$('.ut-login-content > button.call-to-action').first().hasClass('disabled');
    });

    await page.click('.ut-login-content button.call-to-action');
  }

  async function signIn(){
    console.log('Finished login button click redirection');
    await page.screenshot({path: 'finished_login_redirection.png'});

    // Email & PW
    await page.$eval('input#email', el => el.value = '' ); // clear any cookie saved emails
    await page.type('input#email', process.env.ORIGIN_USER, {delay: 100});
    await page.type('input#password', process.env.ORIGIN_PASS, {delay: 100});

    await Promise.all([
      page.waitForNavigation({waitUntil: ['domcontentloaded', 'networkidle0']}),
      page.click('a#btnLogin'),
    ]);

    console.log('Finished credentials redirection');

    await page.screenshot({path: 'finished_credentials_redirection.png'});

    let postSignInState = await Promise.race([
      waitForHome(),
      page.waitForSelector('a#btnSendCode')
    ]);

    if(postSignInState != 'home'){
      fillSecurityCode();

      let postSecurtityCodeState = await Promise.race([
        waitForHome(),
        page.waitForFunction(function(){
          return $('.ut-login-content > h2').text().trim() == 'FUT Security Question'
        })
      ]);

      if(postSecurtityCodeState != 'home'){
        answerSecurityQuestion();
      }
    }
  };

  async function waitForLoginButton(){
    await page.waitForFunction(function(){
      loginButton = document.querySelector('.ut-login-content > button.call-to-action');
      return loginButton && !loginButton.classList.contains('disabled')
    })
    return 'login';
  };

  async function waitForHome(){
    await page.waitForFunction(function(){
      title = document.querySelector('section.FUINavigation > .NavigationBar > h1.title');
      return title && title.innerText == 'HOME';
    })
    return 'home';
  };

  async function waitForSignInPage(){
    await page.waitForFunction(function(){
      signInTitle = document.querySelector('#panel-login > div.login-form-container > div.panel-contents > div > p');
      return signInTitle && signInTitle.innerText == "Sign in with your EA Account";
    })
    return 'sign_in';
  }

  browserConfig = {
    userDataDir: "./user_data" // allow localStorage and cookies to be reused
  }

  if(process.env.DEBUG_MODE == 'true'){
    browserConfig.headless = false;
    browserConfig.devtools = true;
  }

  const browser = await puppeteer.launch(browserConfig);

  const page = await browser.newPage();

  page.on('console', msg => {
    console.log(msg.text());
  });

  page.on("pageerror", function(err) {
    console.error("Page error: ");
    console.error(err.toString());
  });

  page.on("requestfailed", function(err) {
    console.error("Request failed: ");
    console.error(err.toString());
  });

  try{
    await page.setViewport({
      width: 1024,
      height: 768
    });

    await page.goto('https://www.easports.com/fifa/ultimate-team/web-app/');

    // wait for login button to enable, or home arrival
    let waitResult = await Promise.race([waitForLoginButton(), waitForHome()]);

    console.log(`wait result: ${waitResult}`);

    // perform login actions
    if(waitResult == 'login'){
      console.log('Login button is ready');

      // click login button and redirect to login page
      await Promise.all([
        // page.waitForNavigation(),
        page.click('.ut-login-content button.call-to-action'),
      ]);
    }

    // perform security checks
    // TODO: check for login page DOM elements
    waitResult = await Promise.race([waitForHome(), waitForSignInPage()]);

    if(waitResult == 'sign_in'){
      await signIn();
    }

    // wait for home arrival
    await waitForHome();

    // arrived home
    console.log('Arrived Home');
    // inject AutoBuyer to browser context and start the script
    await page.addScriptTag({path: './auto_buyer.js'});

    await page.evaluate(function(){

      console.log('Initializing buyer script');

      var buyer = new AutoBuyer();
      buyer.start();

      return new Promise(function(resolve, reject){
        setTimeout(function(){
          resolve('done');
        }, 12*60*60*1000);
      }); // so pupeteer always wait for us
    });
  }catch(e){
    handleGenericError(e);
  };

  if(process.env.DEBUG_MODE != 'true'){
    // do not close browser if in debug mode, allow further inspections
    await browser.close();
  }
})();