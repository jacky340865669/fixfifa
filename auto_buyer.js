class AutoBuyer {
  constructor(){
    this.NUCLEUS_ID = services.User.getUser().id.toString(); // player id eqv.

    this.REST_INTERVAL_SECONDS = 30*60; // wait time when rest state is hit
    this.REST_CHANCE_PERCENTAGE = 0; // percentage (0-100) to enter rest state between searches

    this.FIXED_INTERVAL_SECONDS = 45; // min. wait time between any search

    this.TARGETS = [
      // { lev: 'silver', leag: 4, maxb: 500},
      { lev: 'silver', team: 231,     maxb: 1200 }, // club brugge
      { lev: 'silver', team: 1806,    maxb: 1000 }, // stoke
      { lev: 'silver', team: 109,     maxb: 750 }, // west brom
      { lev: 'silver', team: 2,       maxb: 750 }, // villa
    ]

    this.SELL_PRICES = {
      139533: 4200,
      163303: 3100,
      169721: 4600,
      173735: 4400,
      175789: 2900,
      175932: 4500,
      181483: 3300,
      186116: 3200,
      191005: 4500,
      193849: 3400,
      210881: 3300,
      216433: 7100,
      222156: 4000,
      231485: 4500,
      6826: 7000,
      52306: 9000,
      119233: 5000,
      162981: 5000,
      186158: 1600,
      189059: 6600,
      203751: 1900,
      225632: 2200,
      231406: 4300,
      231408: 1300,
      233957: 4600,
      234742: 4400,
      51257: 1600,
      138782: 3100,
      155976: 3400,
      158372: 3000,
      164853: 1100,
      178567: 6600,
      193884: 9900,
      198489: 10000,
      200521: 9400,
      202052: 1000,
      204289: 1000,
      204519: 5900,
      205818: 3800,
      234457: 4500,
      147579: 9200,
      176137: 7200,
      188727: 3400,
      191627: 4000,
      205386: 3800,
      208364: 3800,
      209780: 3500,
      213323: 3500,
      219736: 4000,
      224158: 5800,
      228942: 6400,
      234791: 3500,
      235202: 4500,
      236276: 9000,
      237209: 3500,
      238227: 5500,
      239015: 4100,
      239593: 4000,
      243538: 4000,
    }

    // in target, but exclude these players
    this.BYPASS_PLAYER_IDS = [
      // Stoke City
      207633,
      164853,
      204289,
      202052,
      51257,

      // West brom
      184480,
      222104,
      51261,

      // aston villa
      231743,
      215374,
      231352,
      162054,
      155979,
      224953,
      185427
    ];

    this.jobDone = false; // manually set it to true if u wanna stop the bot
  }

  randomInterval(){
    let isRestMode = Math.random() > (1.0 - (this.REST_CHANCE_PERCENTAGE / 100.0 ));
    if(isRestMode){
      console.log('Enter REST mode');
      return this.REST_INTERVAL_SECONDS * 1000;
    }else{
      // randomly add up to 30% of wait time to behave more like authentic user
      return this.FIXED_INTERVAL_SECONDS * 1000 * 0.3 * Math.random();
    }
  }

  sample(samples){
    return samples[Math.floor(Math.random() * samples.length)];
  }

  getHeaders(){
    return {
      Accept: 'text/plain, */*; q=0.01',
      'X-UT-SID': services.Authentication.getSession(enums.AuthEnvironment.UTAS).id,
      'Easw-Session-Data-Nucleus-Id': this.NUCLEUS_ID,
      'Content-Type': 'application/json'
    }
  }

  async searchPlayers(){
    // create event to avoid bot detection (pure guess)
    services.PIN.sendData(enums.PIN.EVENT.PAGE_VIEW, {
      type: PIN_PAGEVIEW_EVT_TYPE,
      pgid: 'Transfer Market Search'
    });

    return $.ajax({
      method: 'GET',
      url: 'https://utas.external.s2.fut.ea.com/ut/game/fifa19/transfermarket',
      data: Object.assign(this.sample(this.TARGETS), {
        macr: 15000000 - Math.floor(Math.random() * 10000) * 1000,
        start: 0,
        num: 21,
        type: 'player',
        '_': new Date().getTime()
      }),
      headers: this.getHeaders()
    })
  }

  async buyPlayer(tradeId, bid){
    return $.ajax({
      method: 'PUT',
      url: 'https://utas.external.s2.fut.ea.com/ut/game/fifa19/trade/'+tradeId+'/bid',
      data: JSON.stringify({bid: bid}),
      headers: this.getHeaders()
    })
  }

  listPlayer(sellPrice, purchaseItemData){
    setTimeout(()=>{
      // move to transfer list
      $.ajax({
        method: 'PUT',
        url: 'https://utas.external.s2.fut.ea.com/ut/game/fifa19/item',
        data: JSON.stringify({
          itemData: [
            {
              id: purchaseItemData.itemData.id,
              pile: 'trade'
            }
          ]
        }),
        headers: this.getHeaders()
      }).done(()=>{
        // put into transfer market
        $.ajax({
          method: 'POST',
          url: 'https://utas.external.s2.fut.ea.com/ut/game/fifa19/auctionhouse?sku_b=FFT18',
          data: JSON.stringify({
            itemData: {
              id: purchaseItemData.itemData.id
            },
            startingBid: (sellPrice - 100),
            duration: 3600,
            buyNowPrice: sellPrice
          }),
          headers: this.getHeaders()
        }).done((response)=>{
          console.log('Done listing player');
          this.cleanSold();
        });
      });
    },1000);
  }

  // remove sold players to avoid transfer list getting full
  cleanSold(){
    $.ajax({
      method: 'DELETE',
      url: 'https://utas.external.s2.fut.ea.com/ut/game/fifa19/trade/sold',
      headers: this.getHeaders()
    }).done(()=>{
      console.log('Sold players removed from list');
    });
  }

  async init(){
    console.log('Searching Players...');
    try{
      let players = await this.searchPlayers();
      var totalPlayers = players.auctionInfo ? players.auctionInfo.length : 0;

      console.log(totalPlayers + ' players found');

      if(totalPlayers > 0){
        for(var i = 0; i < players.auctionInfo.length; i++){
          var player = players.auctionInfo[i];

          if(this.BYPASS_PLAYER_IDS.includes(player.itemData.assetId)){
            console.log('player bypassed');
            continue; // skip if player blacklisted
          }

          if(this.jobDone){ break; }

          try{
            let purchaseData = await this.buyPlayer(player.tradeId, player.buyNowPrice)
            console.log('Done buying player');
            console.log(JSON.stringify(purchaseData));

            let sellPrice = this.SELL_PRICES[player.itemData.assetId]
            if( sellPrice ){
              this.listPlayer(sellPrice, purchaseData.auctionInfo[0]);
            }

            break; // do not try to buy more than one per search to avoid too many buys
          }catch(e){
            // failed to buy
          }
        }
      }
      if(!this.jobDone){
        setTimeout(()=>{
          this.init()
        }, this.randomInterval() + (this.FIXED_INTERVAL_SECONDS * 1000) );
      }
    }catch(e){
      console.error(JSON.stringify(e));
      console.error('Something went wrong, bot aborted');
    }
  }

  stop(){
    this.jobDone = true;
  }

  start(){
    this.jobDone = false;
    console.log('Buyer starting in 3s...');
    setTimeout(()=>{
      this.init();
    }, 3000);
  }
}
